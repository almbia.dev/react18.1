import PropTypes from "prop-types";
import TodoItem from "./TodoItem.jsx";



function TodoList(props) {
  const { todos } = props;
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
        {todos.map(function  (task){
          return (
          <TodoItem
            key={task.id}
             title={task.title} 
             completed={task.completed}
             ></TodoItem>)})}
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
